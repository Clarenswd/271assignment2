function validate() {
    return checkName();

}


function checkName(){
var name = document.getElementById("name");
    
    if (name.value==""){
        document.getElementById("nameMissing").style.visibility = "visible";
        name.focus();    
        return false;
    }else{
		document.getElementById("nameMissing").style.visibility = "hidden";
		}
    return checklastName();

}

function checklastName(){
    var lastname = document.getElementById("lastname");
    
    if (lastname.value==""){
        document.getElementById("surnameMissing").style.visibility = "visible";
        lastname.focus();
        return false;
    }else{
		document.getElementById("surnameMissing").style.visibility = "hidden";
		}
    return checkPhone();

}

function checkPhone(){
    var phone = document.getElementById("phone");
    var v = /^$/.test(phone.value);
    
    if(v){//if v is true, it means that the regEx matched.
        document.getElementById("phoneMissing").style.visibility = "visible";
		phone.focus();
        return false;
    }else{
		document.getElementById("phoneMissing").style.visibility = "hidden";
		}
    return checkSaddress();
}


function checkSaddress(){
    var address = document.getElementById("saddress");
    var e = /^$/.test(address.value);
    if(e){
		document.getElementById("saddressMissing").style.visibility = "visible";
		address.focus();
        return false;
    }else{
		document.getElementById("saddressMissing").style.visibility = "hidden";
		}
    return checkEmail(); 
}

function checkEmail(){
    var email = document.getElementById("email");
	//var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	
    //var e = re.test(email);
	//var re = /^$/.test(email.value);
	//check validation
	//alert(e);
    if(email.value == ""){
		document.getElementById("emailMissing").style.visibility = "visible";
		email.focus();
        return false;
	}else{
	document.getElementById("emailMissing").style.visibility = "hidden";
	}
    return checkState(); 
}


function checkState(){
    var state = document.getElementById("state");
    if(state.selectedIndex==0){
		document.getElementById("stateMissing").style.visibility = "visible";
		state.focus();
        return false;
    }else{
		document.getElementById("stateMissing").style.visibility = "hidden";
		}
    return checkUsername();

}

 


 
function checkUsername(){
    var username= document.getElementById("username");
    //if the value of username matches the pattern, test() returns TRUE
    var empt = /^$/.test(username.value);
    
    if(empt){
		document.getElementById("usernameMissing").style.visibility = "visible";
		username.focus();
        return false;
    }else{
		document.getElementById("usernameMissing").style.visibility = "hidden";
		}
    return checkPassword();
}

function checkPassword(){
    var passw1 = document.getElementById("password");
    var passw2 = document.getElementById("checkPassword");
    
    if (/^$/.test(passw1.value )){
		document.getElementById("passwordMissing").style.visibility = "visible";
		document.getElementById("doNotMatch").style.visibility = "hidden";
		passw1.focus();
        return false;
    }else if(/^$/.test(passw2.value )){
		document.getElementById("checkPasswordMissing").style.visibility = "visible"; 
		document.getElementById("doNotMatch").style.visibility = "hidden";
		passw2.focus();
        return false;
    }
	
	if (passw1.value != passw2.value){
		document.getElementById("checkPasswordMissing").style.display = "none"; 
		document.getElementById("passwordMissing").style.visibility = "hidden";
		document.getElementById("doNotMatch").style.visibility = "Visible";
        return false;
    }   
    return true;

}

/*
Before every return false; we have to change the state of some 
span that alerts the user what was done wrong, instead of using alerts.
*/



/*
The following funciton should receive a parameter
as the Id of the span! that would be sweet
*/

function hideSpan(){

    document.getElementById("surnameMissing").style.visibility="hidden";
}
