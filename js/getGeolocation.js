function grabLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition2, showError);
	} else {
		document.getElementById("status").innerHTML="Geolocation is not supported by this browser.";
	} 
}

function showPosition2(position) {
	//document.getElementById("status").innerHTML = "Latitude: " + position.coords.latitude + ", Longitude: " + position.coords.longitude;
	document.getElementById("latitude").value = position.coords.latitude;
	document.getElementById("longitude").value = position.coords.longitude;
	document.getElementById("location_form").submit();	
	
	var latitudex = position.coords.latitude;
	var longitudex = position.coords.longitude;
	
	//lat and long from the item in DB
	var latitudeDis =28.418611;
	var longitudeDis =-81.581111;
	
	var R = 6371; // km
	var dLat = (latitudex-latitudeDis).toRad();
	var dLon = (longitudex-longitudeDis).toRad();
	var latitudeDis = latitudeDis.toRad();
	var latitudex = latitudex.toRad();

	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
			Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(latitudeDis) * Math.cos(latitudex); 
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	var d = R * c;
	//document.write("The answer:".d);
	document.getElementById("status").innerHTML = d;

	// display on a map
	var latlon = position.coords.latitude + "," + position.coords.longitude;
	var img_url = "http://maps.googleapis.com/maps/api/staticmap?center="+latlon+"&zoom=14&size=400x300&sensor=false";
	document.getElementById("mapholder").innerHTML = "<img src='"+img_url+"'>";
	
}
function showError(error) {
	var msg = "";
	switch(error.code) {
		case error.PERMISSION_DENIED:
			msg = "User denied the request for Geolocation."
			break;
		case error.POSITION_UNAVAILABLE:
			msg = "Location information is unavailable."
			break;
		case error.TIMEOUT:
			msg = "The request to get user location timed out."
			break;
		case error.UNKNOWN_ERROR:
			msg = "An unknown error occurred."
			break;
	}
	document.getElementById("status").innerHTML = msg;
}