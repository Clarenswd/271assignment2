<?php
session_start();
include ('functions/connection.inc');
include ('functions/queries.php');
include('template.inc');
 
if(isset($_GET['id'])){
	$id = $_GET['id'];
}else{
	header("Location: index.php");
}	

	header_template("IndividualPage");
	topNavigation();


	
?>

        <div class="mainWrap">
         	<?php
			queryIndividual($pdo, $id);
			?>
		<!--The following div, should have an infinite height(as long as is needed)-->
		<div id="list-of-reviews">	
		<?php
			//function to pull the reviews from the DB
			showReviews($pdo,$id);
		?>

        </div>
	

<?php	if (isset($_SESSION['isAdmin'])||isset($_SESSION['isNormalUser'])) {?>
        <div id="make-review-panel">
		<form action="sendReview.php" method="post">
		<input type="hidden" name="user_id" value="<?php echo $_SESSION['userId'];?>"/>
		<input type="hidden" name="item_id" value="<?php echo $id;?>"/>
		
        <span id="title-insertion">Make a review!</span>   
			<table id="insert_review">
				<tbody>
				<tr>
					<td>Review:</td> <td><textarea name="text"></textarea></td><td>Stars:<select name="rating"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select></td>
				</tr>
				<tr>
					<td colspan="3"><input type="submit" id="insert_review_btn" value="Save review" /></td>
				</tr>
				</tbody>
			</table>
		</form>
		</div>
<?php } else{echo "Do you want to write a review for this item?<br/> You are one step away from it! <a href='login.php'>login</a> or <a href='sign-up.php'>sign - up</a>";}?>          
        </div><!--mainWrap-->
        
       
        
 <?php
footer_template();
?>		
 