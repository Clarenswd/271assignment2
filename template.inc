<?php
function header_template($title="271 - Assignment 2"){
?>
<!doctype html>

<html>
    <head>
    <script type="text/javascript" src="js/jquery.min.js"></script>      
    <title><?php echo $title;?></title>
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
	<!--Apple-->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
  	<link rel="apple-touch-icon" href="img/apple-icon.png"/>
    <link rel="apple-touch-startup-image" href="img/splash.png" />
    <!--Android-->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="196x196" href="img/android-icon.png">
    
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/fonts-stylesheet.css">
  
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/getGeolocation.js"></script>
	<script type="text/javascript" src="js/validate.js"></script>

	  
	  
	  <!--[if lt IE 7]>
        <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
      <![endif]-->
	  
      <!--[if lt IE 9]>
      <script src="dist/html5shiv.js"></script>
      <![endif]-->
      
       <!--[if lt IE 8]>
      <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
      <![endif]-->


    </head>
	
    <body>
           
        <div id="container">
        
        
   <?php 
} 
function topNavigation(){
?>
		<header>
           <span id="logo-text"><img src="img/logo.png" class="logo-img" alt="public art in Brisbane - logo"/>Public Art <span class="small">in Brisbane</span> </span> 
			<nav>
           	   <span id="menu-text" class="nav-btn"></span>
               <ul id="link-list">
                  <li><a href="index.php">Home </a></li>
                  <li> |</li>
                  <li><a href="index.php#mainContent">Search </a></li>
                  <li> |</li>
                  <li><a href="sign-up.php">Sign up</a></li>
                  <li> |</li>
				  <li><a href="info.txt">Info </a></li>
                  <li> |</li>
				  <?php if (isset($_SESSION['isAdmin'])||isset($_SESSION['isNormalUser'])) {?>
				  
				  <li><a href="logout.php">Log out </a></li>
				  <?php } else{?>
				  <li><a href="login.php">Login </a></li>
				  <?php
				  }?>
                   
				  
                </ul>
           </nav>
        </header>
<?php
}

function footer_template(){
?>

  <footer>
        
        <span class="col left">
        	<img src="img/logo.png" class="logo-img"  alt="logo of public art in Brisbane" /><p><strong>Public Art</strong> is a web created for you, the artist that is looking for inspiration, the artist that want to see and appreciate other's works. Most people don't speak in latitudes and longitudes; they denote locations using addresses, that is why we facilitate the rating of public art by providing an accurate location via Google maps API. We believe that design is in everything and the more one sees something the more realizes that art is involved. </p>
        </span>
 
        <span class="col middle">
        <span><strong><i>feeling Social?</i></strong></span><br/><br/>
        	<a href="www.facebook.com"><img src="img/facebook.png" alt="facebook logo"/></a><br/>
            <a href="www.facebook.com"><img src="img/instagram.png" alt="instagram logo"/></a> <br/>
            <a href="www.facebook.com"><img src="img/youtube.png" alt="youtube logo"/></a><br/>
        </span>
 
        <span class="col right">
        	<span><strong>Useful Links</strong></span>
        	<ul>
                   <li><a href="index.html">Home </a></li>
                  <li><a href="index.html#mainContent">Search</a></li>               
				  <li><a href="#">News </a></li>
				  <li><a href="#">Users </a></li>
				  <li><a href="#">Contact Us</a></li>
        	</ul>
        </span>
            
           <div id="copyright"><p>Copyright ClarensWD QUT - INB271 - 2014</p></div>
        </footer>
        
        </div><!--container ends-->
    
    </body>
</html>
 <?php
 }
 
 function successScreen(){
 ?>
 <div class="mainWrap">
 <br/><br/>
 <h1>Success, your account has been created</h1>
 <h2><i>... now you can Log in!</i></h2><br/>
 <a href="login.php">login</a>
 </div>
 
 <?php
 }
 
 ?>