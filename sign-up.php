<?php
session_start();
include('template.inc');
include('functions/connection.inc');
include('functions/general.php');
header_template("SignUp");
topNavigation();


$name  		= ""; 
$lastname  	= "";
$phone  	= "";
$saddress  	= "";
$email		= "";
$username  	= "";
$password  	= "";
$checkPassword ="";

$error_msg = "";
	



function insertion($pdo, $name, $lastname, $phone, $saddress, $email, $state, $username, $password){


	$query = $pdo->prepare("INSERT INTO members (first_name, last_name, phone, saddress, email, state, username, password) ".
			"VALUES (:first_name,:last_name, :phone, :saddress, :email, :state, :username, SHA2(:password,0))");
	
	$query->bindValue(':first_name',$name);
	$query->bindValue(':last_name',$lastname);
	$query->bindValue(':phone',$phone);
	$query->bindValue(':saddress',$saddress);
	$query->bindValue(':email',$email);
	$query->bindValue(':state',$state);
	$query->bindValue(':username',$username);
	$query->bindValue(':password',$password);
	$query->execute();
	
	if($query->rowCount()>0){
		successScreen();
	}else{
	echo "Problems";
	}

}
 

$selected[0] = createoptions("Please select","","selected");
$selected[1] = createoptions("Queensland", "QLD", "");
$selected[2] = createoptions("Northern Territory", "NT", "");
$selected[3] = createoptions("New South Wales", "NSW", "");
$selected[4] = createoptions("South Australia", "SA", "");
$selected[5] = createoptions("Western Australia", "WA", "");
$selected[6] = createoptions("Western Australia", "QLD", "");
$selected[7] = createoptions("Victoria", "VIC", "");
$selected[8] = createoptions("Australian Capital Territory", "ACT", "");
$selected[9] = createoptions("Tasmania", "TAS", "");

if($_POST){	
	$name  		= $_POST['name'];
	$lastname  	= $_POST['lastname'];
	$phone  	= $_POST['phone'];
	$saddress  	= $_POST['saddress'];
	$email	 	= $_POST['email'];
	$state 		= $_POST['state'];
	$username  	= $_POST['username'];
	$password  	= $_POST['password'];
	$checkPassword  = $_POST['checkPassword'];
	$createBtn = $_POST['create-btn'];

	checkPostEmpty($_POST);
	if(chkPasswd($password, $checkPassword)){
		insertion($pdo, $name, $lastname, $phone, $saddress, $email, $state, $username, $password); 
	}else{
		$error_msg = " New user could not have been created";
	}
}
?>
	<div class="mainWrap">
           
        	<div id="sign-up">
			
            <form name="" action="sign-up.php" id="formCreate" method="post" onsubmit="return validate()">
            <fieldset>
            
            <legend>Create User</legend>
            <label for="name">Name:</label>
            <input type="text" id="name" name="name" value="<?php echo $name;?>"  placeholder="Given name"  >
            <span id="nameMissing"  class="missing">Name	is	a	required	field</span>
            <br>
            
            <label for="lastname">Last name:</label>
            <input type="text" id="lastname" value="<?php echo $lastname;?>" name="lastname" placeholder="Family name"  >
            <span id="surnameMissing"   class="missing">Surname	is	a	required	field</span>
            <br>
            
            <label for="phone">Phone</label>
            <input type="tel" id="phone" name="phone" value="<?php echo $lastname;?>" placeholder="0000 000 000"   >
            <span id="phoneMissing"  class="missing">Phone is a required field</span>
            <br>

            <label for="saddress">Street address</label>
            <textarea name="saddress" id="saddress" cols="30" rows="2"  ><?php echo $saddress;?></textarea>
            <span id="saddressMissing"  class="missing">Street address is a required field</span>
            <br>
            
			<label for="saddress">Email</label>
            <input type="text" name="email" id="email"  >
            <span id="emailMissing"  class="missing">email address is invalid</span>
            <br>
             
            <label for="state">State:</label>
            <select name="state" id="state"  >
				<?php 
				foreach($selected as $options){
					echo $options;
				}
				?>		
               
            </select>
			
			
			
            <span id="stateMissing" class="missing">State is a required field</span>
            <br>
            <label for="username">Username</label>
            <input type="text" id="username" name="username"  value="<?php echo $username;?>">
            <span id="usernameMissing"  class="missing">Username is a required field</span>
            <br>
            <label for="password">Password</label>
            <input type="password" id="password" name="password" value="<?php echo $password;?>" >
            <span id="passwordMissing"  class="missing">Password is a required field</span>
            <br>
            <label for="checkPassword">Repeat Password</label>
            <input type="password" id="checkPassword" name="checkPassword" value="<?php echo $checkPassword;?>" >
            <span id="checkPasswordMissing"  class="missing">Re-type the password, it is required</span>
            <span id="doNotMatch" class="missing">Passwords do not match!</span>
            <br>
            <input type="submit" id="create-btn"  name="create-btn"  value="Create user!">
            </fieldset>
        </form>
              <h4 class="error"><?php echo $error_msg;?></h4>	  
            </div>

          
        </div><!--mainWrap-->
        
       
<?php
footer_template();
?>		
 
 