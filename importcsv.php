<?php 
	require 'adminPermission.inc'; 
	include ('functions/connection.inc');
	include('template.inc');
	header_template("Import CSV into DB - AREA 51");
	
	topNavigation();
	
	if(isset($_POST['submit'])){
		$file = $_FILES['file']['tmp_name'];
		$handle = fopen($file,"r");
		while(($fileop = fgetcsv($handle,1000,","))!==false){
			$primary_maker=$fileop[0];
			$name_title=$fileop[1];
			$brief_description=$fileop[2];
			$media_materials=$fileop[3];
			$production_date=$fileop[4];
			$measurement=$fileop[5];
			$installation_info=$fileop[6];
			$object_type=$fileop[7];
			$public_art_subject=$fileop[8];
			$public_art_theme=$fileop[9];
			$street_location=$fileop[10];
			$suburb=$fileop[11];
			$latitude_export=$fileop[12];
			$longitude_export=$fileop[13];
 
			$query = $pdo->prepare("insert into items2 (primary_maker, name_title, brief_description, media_materials, ".
			"production_date, measurement, installation_info, object_type, public_art_subject, public_art_theme,".
			"street_location, suburb, latitude_export, longitude_export)".
			"values (:primary_maker, :name_title, :brief_description, :media_materials, :production_date,".
			":measurement, :installation_info, :object_type, :public_art_subject, :public_art_theme, ".
			":street_location, :suburb, :latitude_export, :longitude_export)");
			
			$query->bindValue(':primary_maker',$primary_maker);
			$query->bindValue(':name_title',$name_title);
			$query->bindValue(':brief_description',$brief_description);
			$query->bindValue(':media_materials',$media_materials);
			$query->bindValue(':production_date',$production_date);
			$query->bindValue(':measurement',$measurement);
			$query->bindValue(':installation_info',$installation_info);
			$query->bindValue(':object_type',$object_type);
			$query->bindValue(':public_art_subject',$public_art_subject);
			$query->bindValue(':public_art_theme',$public_art_theme);
			$query->bindValue(':street_location',$street_location);
			$query->bindValue(':suburb',$suburb);
			$query->bindValue(':latitude_export',$latitude_export);
			$query->bindValue(':longitude_export',$longitude_export);
			try {
			$query->execute();
			}catch(PDOException $e){
			  echo "Exception caught: $e";
			}
			
		}
		
		if($query->rowCount()>0){
			echo "The data was successfully imported to the database!";
		}else{
			echo "Nothing happened, check the log file!";
		}
	 
	
	}
	
	
	
?>

        <div class="mainWrap">

        	<div id="mainContent"><br/><br/><br/>
				<form action="importcsv.php" method="post" enctype="multipart/form-data">
						<label for="file">Choose a file to be imported to the database:</label>
						<input type="file" name="file">
						<br/><br/>
						<input type="submit" name="submit" value="import">        
				</form>
			</div>

		</div>


<?php
	footer_template();	
?>		