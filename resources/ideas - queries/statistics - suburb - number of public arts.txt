/*
Shows a list of suburbs with a number of public arts.
The following query could be used for a statistics admin page!

*/

SELECT DISTINCT suburb, COUNT( * ) 
FROM  `items` 
GROUP BY suburb
LIMIT 30 , 30
