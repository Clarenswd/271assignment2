<?php
/*sendReview page that receives the parameters from 
individual page.
Parameters: 
user_id: the user making the review
text: text review
id: item id
rating: number of stars
*/
if($_POST){
include('functions/general.php');
include("functions/connection.inc");
$error_msg="";
session_start();
checkPostEmpty($_POST);

$user_id= $_POST['user_id'];
if($_SESSION['userId'] == $user_id){
$text = $_POST['text'];
//$date_posted= $_POST['date_posted'];
$rating = $_POST['rating'];
$item_id =$_POST['item_id'];
try{/*NEED TO GET THE DATE!!*/
	$query = $pdo->prepare("insert into review (user_id, item_id, date_posted, text, rating) values(:user_id, :item_id, NOW(), :text, :rating)");
	$query->bindValue(":user_id",$user_id);
	$query->bindValue(":item_id",$item_id);
	//$query->bindValue(":date_posted",$date_posted);
	$query->bindValue(":text",$text);
	$query->bindValue(":rating",$rating);
	$query->execute();
	if($query->rowCount()>0){
		header("refresh:3;url=individual.php?id=$item_id");
		echo "Thanks, your review was inserted, you will shortly be redirected!";
	}else{
		echo "Problems with insertion";
	}

}catch(PDOException $e){
	return false;
}


}else{
die("Access denied");
}

}else{
	header("Location: index.php");
}


