<?php
/*
Script in charge of authentication.
For this assignment, we set the user_id number 1 to be an administrator.

*/


function checkPassword($username,$passwd){
include("connection.inc");
try {
		$stmt = $pdo->prepare("SELECT * FROM members WHERE username = :username and password = SHA2(:password, 0) "); 
		$stmt->bindValue(':username', $username); 
		$stmt->bindValue(':password',$passwd);
		$stmt->execute(); 
		return $stmt->rowCount() > 0;	
	}catch (PDOException $e){
		return false;
	}

}


function getUserId($username, $passwd){
include("connection.inc");
try{
	$stmt= $pdo->prepare("select user_id from members where username = :username and password = SHA2(:password,0)"); 
	$stmt->bindValue(':username', $username);
	$stmt->bindValue(':password',$passwd);
	$stmt->execute();
	foreach($stmt as $result){
	return $result['user_id'];
	}
	
}catch(PDOException $e){
	return false;
}


}



if (isset($_POST['login'])) 
 { 
	// validate and process posted username and password here 
	if (checkPassword($_POST['username'], $_POST['password'])) {
		$theUserId= getUserId($_POST['username'], $_POST['password']); 
		session_start(); 
		
		$_SESSION['userId'] = $theUserId; 
		if ($theUserId ==1)
		{
		$_SESSION['isAdmin'] = true; 
		header("Location: /271web2/private.php"); 
		}else{
		$_SESSION['isNormalUser'] = true;
		header("Location: /271web2/index.php"); 
		}
		//header("Location: https://{$_SERVER['HTTP_HOST']}/~n8898642/a2/private.php"); 
		
		exit();
	}else{
		//header("Location: https://{$_SERVER['HTTP_HOST']}/~n8898642/a2/login.php?error=err1"); 
		header("Location: /271web2/login.php?error=err1"); 
		}
 } 

?>