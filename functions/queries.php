<?php
//accept one third parameter that checks if you 
//show a brief list of the item or a massive chunk of info

function queryIndividual($pdo, $id){
$query = 	$pdo->prepare("SELECT * FROM  items WHERE id = :id");
			$query->bindValue(':id',$id);
			$query->execute();
	if($query->rowCount()>0){
	foreach($query as $result){
	echo 	"<div id='results-section'>".
				"<div id='the-table'>".
				"<div id='name-art'><span class='tag'>Name:</span> {$result['name_title']}  </div>".
				"<div id='maker-art'><span class='tag'>Maker:</span> {$result['primary_maker']}</div>".             			
				"<div id='date-art'><span class='tag'>Date:</span>{$result['production_date']}</div>".
				"<div id='measurement-art'><span class='tag'>Measurement:</span>{$result['measurement']}</div>".  
				"<div id='installation-art'><span class='tag'>Installation Information:</span>{$result['installation_info']}</div>".
				"<div id='objectType-art'><span class='tag'>Object Type:</span>{$result['object_type']}</div>".
				"<div id='subject-art'><span class='tag'>Public Art Subject:</span>{$result['public_art_subject']}</div>".
				"<div id='theme-art'><span class='tag'>Public Art Theme:</span>{$result['public_art_theme']}</div>".
				"<div id='street-art'><span class='tag'>Street Location:</span>{$result['street_location']}</div>".
				"<div id='suburb-art'><span class='tag'>Suburb:</span>{$result['suburb']}</div>".
				"<div id='latitude-art'><span class='tag'>Latitude:</span>{$result['latitude_export']}</div>".
				"<div id='longitude-art'><span class='tag'>Longitude:</span>{$result['longitude_export']}</div>".
				"<div id='geLocation'><button onclick='getLocation({$result['latitude_export']}, {$result['longitude_export']})' id='geo-btn'>show it on the map!</button><p id='status'>&nbsp;</p></div>".
				"<div id='mapholder'></div>".
				"</div>".    
            "</div>";
			
	}
	}else{
		echo "No results :) Unfortunatelly something happen during the process :( ";
	}


}



function queryView($query, $search_field){

	
	$query->execute();
	 
	if($query->rowCount()>0){
		resultsTableHead();

			foreach($query as $result){
				echo 	"<tbody>".
						"<tr>".
						"<td class='bold-col'>Name:</td>".
						"<td>{$result['name_title']}</td>".
						"</tr>".
						"<tr>".
						"<td class='bold-col'>Maker:</td><td>{$result['primary_maker']}</td>".
						"</tr>".
						"<tr><td class='bold-col'>Suburb:</td><td>{$result['suburb']}</td></tr>".
						"<tr><td class='bold-col'>Object type:</td><td>{$result['object_type']}</td></tr>".
						"<tr><td class='bold-co'>Description:</td><td colspan='2'>{$result['brief_description']}</td></tr>".      
						"<tr><td colspan='2'><a href='individual.php?id={$result['id']}'>read more...</a></td></tr>".
						"</tbody>";

						
						
						
						
			}
		resultsTableFooter();
	}else{
		echo "No results for $search_field";
	}

}



function queryByName($pdo, $search_field){

	$query = $pdo->prepare("SELECT * FROM  items WHERE name_title LIKE :search_field OR object_type LIKE :search_field");
	$query->bindValue(':search_field',"%".$search_field."%");
	queryView($query , $search_field);
}

function queryBySuburb($pdo, $search_field){
	$query =  	$pdo->prepare("SELECT * FROM  items WHERE suburb LIKE :search_field");
	$query->bindValue(':search_field',"%".$search_field."%");
	queryView($query , $search_field);
}
function queryByRating($pdo, $search_field){

	$query = $pdo->prepare("SELECT * FROM review r, items i WHERE r.item_id = i.id AND r.rating = :search_field");
	$query->bindValue(':search_field',$search_field);
	queryView($query, $search_field." stars as a rating!");

}
function queryByLocation($pdo, $search_field){

	//grab your location through $search_field
	//grab all the items with location within 3 km of your current location... theorically that would work //POW(3,2)
	$query = $pdo->prepare("SELECT * FROM items WHERE SQRT(POW(latitude_export - :lati , 2) + POW(longitude_export - :longi, 2))   <  0.05"); 
	$query->bindValue(':lati', $search_field[0]);
	$query->bindValue(':longi', $search_field[1]);

	$search_field =  "this location: ".$search_field[0] ." " .$search_field[1];

	queryView($query, $search_field );
	//shows the results

}

function resultsTableHead(){
?>
 <table  border="0">
   		  <thead>
          	<tr>
            	<th colspan="2" align="center">
                    <h3>Results</h3>
                </th>
              </tr>
          </thead>
<?php
}
function resultsTableFooter(){
	echo "</table>";
}


function showReviews($pdo,$id){

	$query = $pdo->prepare("SELECT * FROM review r, members m WHERE m.user_id = r.user_id AND r.item_id = :item_id");
	$query->bindValue(':item_id',$id);
	$query->execute();
	if($query->rowCount()>0){
		echo "<span><strong>List of reviews!</strong></span>".      	
			"<table>";
		
		
		foreach ($query as $review ){
			echo "<tr>";
			echo "<td><strong>User: {$review['first_name']}</strong></td>";
			echo "<td><i>{$review['text']}</i></td>";
			echo "<td>{$review['date_posted']}</td>";
			echo "<td>";
			for($i=0; $i<$review['rating']; $i++){
				echo '<img src="img/star.png" alt="star" class="star"/>';
			}
			echo "</td>";
			echo "</tr>";
		}
		echo "</table><br/>";
	}else{
		echo "There is no review for this item, you can be the first!!";
	}
}
?>