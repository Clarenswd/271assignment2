<?php
session_start();
include('template.inc');
include('functions/connection.inc');
include('functions/general.php');
	/*
	Header_template --> receives a parameter, which
	is the title for the page. 
	It has a default parameter, in case that we do
	not provide one (271 - Assignment2)
	*/
	header_template();
	
	topNavigation();
?>

		<a href="sign-up.php"><img src="img/public-art_01.png" id="public-art-img" width="100%" alt="image showcasing multiple public arts in Brisbane" /></a><br/>

        <div class="mainWrap">

        	<div id="mainContent">

                <h1>Search by:</h1>
                <section id="search-cloud">
					<a href="#general-search"><span id="top">Name</span></a>
					<a href="#geolocation-search"><span id="left">My location</span></a>
					<a href="#suburb-search"><span id="right">Suburb</span></a>
					<a href="#rating-search"><span id="bottom">Rating</span></a>
                    
                </section><br/>
                <a href="#logo-text" class="other-op-link">Go to the top!</a>
            </div>

            <div id="search-options-frame" >
            
				<!--The following form will send values to query the Database in a more general form.-->
				<section id="general-search" class="tall-section">
				<h1>Name of public art:</h1>
				<form action="search.php" method="post" >
					<input type="hidden" name="type_search" value="by_name"/>
					<input type="text" id="formInput" name="search_field" placeholder="Search"/>
					<input type="submit" id="formButton" value="Search"/>
					
				</form>
				<br>
				<a href="#mainContent" class="other-op-link">other options</a>
				</section>
				
				<!--This form will query the database by suburbs-->
				<section id="suburb-search" class="tall-section">
				<h1>Suburb of public art:</h1>
				<form action="search.php" method="post" >
					<input type="hidden" name="type_search" value="by_suburb"/>
					<!--The user is presented a combo box with a list of Brisbane suburbs-->
					<select id="combo-box" name="search_field" name="search_field"  size="1">
					<?php
					//The list of suburbs will be grab from the existing list in the DB
					//there is no point in wasting user's time by showing options with no results
					listSuburbs($pdo);
					?>
					</select>
					<input type="submit" id="formButton" value="Search"/>
					
				</form>
				<br>
				<a href="#mainContent" class="other-op-link">other options</a>
				</section>
				
				<!--The user will have the option to click on the number of stars that represents certain places-->
				<section id="rating-search" class="tall-section">
				<h1>How many stars?</h1>
				<form action="search.php" method="post" id="star_form"><br/>
					<input type="hidden" name="type_search" value="by_rating"/>
					<input type="hidden" name="search_field" id="search_field" />
					<span id="search-span-text">Search by rating:</span><br/>
					<span>
						<img src="img/star.png" class="star" onClick="send(1)"  /> 
						<img src="img/star.png" class="star" onClick="send(2)"  /> 
						<img src="img/star.png" class="star" onClick="send(3)"  /> 
						<img src="img/star.png" class="star" onClick="send(4)"  /> 
						<img src="img/star.png" class="star" onClick="send(5)"  /> 
					</span> 
					 
					
				</form>
				<br>
				<a href="#mainContent" class="other-op-link">other options</a>
				</section>
				
				<!--Using the location of the user, the results will show places where they can find public arts-->
				<section id="geolocation-search" class="tall-section">
				<h1>Where abouts?</h1>
				<form action="search.php" method="post" id="location_form" ><br/>
					<input type="hidden" name="type_search" value="by_location"/>
					<input type="hidden" id="latitude" name="search_field[]"/>
					<input type="hidden" id="longitude" name="search_field[]"/>
					<span id="search-span-text">Search using your location:</span><br/>
					<img src="img/marker.png" onclick="grabLocation()"/>
						
				</form>
				<br>
				<span id="status"></span>
				<a href="#mainContent" class="other-op-link">other options</a>
				</section>
				
				
				<br/><br/>
				<h1 id="phrase">Search public arts around Brisbane!</h1>   
            </div>
        </div><!--end of mainWrap-->
        
        <div class="mainWrap" >
			<!--Google map uses the following Div-->
			<div id="map"></div>
        </div>

<?php
footer_template();
?>		